# Docker and Docker-compose on ARM

[Docker Compose Networking](https://runnable.com/docker/docker-compose-networking)
[Docker Bridge and Overlay Network with Compose Variable Substitution](http://blog.arungupta.me/docker-bridge-overlay-network-compose-variable-substitution/)

[Running Gitlab CI with Docker](https://medium.com/@augusteo/running-gitlab-ci-with-docker-e64d8af0c82a)
[A guide to automated Docker deployments w/ GitLab CI](https://blog.stackahoy.io/a-guide-to-automated-docker-deployments-w-gitlab-ci-510966dd6022)

[GitLab CI example projects](https://gitlab.com/gitlab-examples)

[Docker & Raspberry Pi, perfect combo!](https://toub.es/2017/07/13/docker-raspberry-pi-perfect-combo/)

[Humidity sensor](http://nagashur.com/blog/2017/06/25/bme280-sur-raspberry-pi-temperature-pression-et-humidite-en-i2c/)


sudo apt-get install docker-ce=18.06.1~ce~3-0~raspbian
https://github.com/umiddelb/armhf/wiki/Get-Docker-up-and-running-on-the-RaspberryPi-(ARMv6)-in-four-steps-(Wheezy)
https://gist.github.com/davidbradway/ecde825a6981d8a7f99d6c7691139f9b

## Update Raspbian

Debian for the Raspberry Pi already comes with a Docker enabled kernel, but you have to run the update on your own first.

```shell
pi@armboard:~ $ sudo apt-get update && sudo apt-get -y upgrade # answer 'y' to upcoming questions 
```
```shell
pi@armboard:~ $ sudo apt-get -y dist-upgrade # answer 'y' to upcoming questions
```
```shell
pi@armboard:~ $ sudo init 6
```
```shell
pi@armboard:~ $ sudo apt-get -y autoremove
```
```shell
pi@armboard:~ $ sudo apt-get -y purge $(dpkg -l | awk '/^rc/ { print $2 }')
```
```shell
pi@armboard:~ $ sudo init 6
```

### Make your raspbian lighter

The official Raspbian comes with a lot of packages pre-installed, many of them don't make sense if you want to run the Raspberry Pi as a server.

```shell
pi@armboard:~ $ sudo apt-get -y remove `sudo dpkg --get-selections | grep -v "deinstall" | grep x11 | sed s/install//`
```
```shell
pi@armboard:~ $ sudo apt-get -y remove `sudo dpkg --get-selections | grep -v "deinstall" | grep sound | sed s/install//`
```
```shell
pi@armboard:~ $ sudo apt-get -y remove `sudo dpkg --get-selections | grep -v "deinstall" | grep gnome | sed s/install//`
```
```shell
pi@armboard:~ $ sudo apt-get -y remove `sudo dpkg --get-selections | grep -v "deinstall" | grep lxde | sed s/install//`
```
```shell
pi@armboard:~ $ sudo apt-get -y remove `sudo dpkg --get-selections | grep -v "deinstall" | grep gtk | sed s/install//`
```
```shell
pi@armboard:~ $ sudo apt-get -y remove `sudo dpkg --get-selections | grep -v "deinstall" | grep desktop | sed s/install//`
```
```shell
pi@armboard:~ $ sudo apt-get -y remove `sudo dpkg --get-selections | grep -v "deinstall" | grep gstreamer | sed s/install//`
```
```shell
pi@armboard:~ $ sudo apt-get -y remove `sudo dpkg --get-selections | grep -v "deinstall" | grep avahi | sed s/install//`
```
```shell
pi@armboard:~ $ sudo apt-get -y remove `sudo dpkg --get-selections | grep -v "deinstall" | grep dbus | sed s/install//`
```
```shell
pi@armboard:~ $ sudo apt-get -y remove `sudo dpkg --get-selections | grep -v "deinstall" | grep freetype | sed s/install//`
```
```shell
pi@armboard:~ $ sudo apt-get -y autoremove
```
```shell
pi@armboard:~ $ sudo apt-get clean
```

### Docker install with remote official bash script

1. Install docker

Since 2016, Docker provides official support for the ARM architecture. To install Docker Engine, logon to your Pi and just type. Install the Docker client on your Raspberry Pi with just one terminal command : 
```shell
pi@armboard:~ $ curl -sSL https://get.docker.com | sh
```

2. Check installed version

If everything goes well, you should be able to check Docker version in console.
```shell
pi@armboard:~ $ docker --version
```

3. Avoid using docker with root

Adding a user to the "docker" group will grant the ability to run containers which can be used to obtain root privileges on the docker host.If you would like to use Docker as a non-root user, you should now consider adding your user to the "docker" group with something like :
```shell
pi@armboard:~ $ sudo usermod -aG docker $USER
```

### Docker install with package (you can specify a version)
1. Required packages first

The first task is to install the required packages and configure the Docker repository.
Lets start with installing the packages.
```shell
pi@armboard:~ $ sudo apt install -y apt-transport-https ca-certificates curl gnupg  gpgv
```

2. Required packages first

Once the packages have been installed the next step is to add the Docker GPG key.
```shell
pi@armboard:~ $ curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
```
3. Add the Docker repository.
```shell
pi@armboard:~ $ echo "deb [arch=armhf] https://download.docker.com/linux/raspbian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list
```

4. Install latest docker available

The latest version
```shell
pi@armboard:~ $ sudo apt-get update && sudo apt-get install docker-ce
```

5. Install a specific version

Get all stable specific available
```shell
pi@armboard:~ $ apt-cache madison docker-ce
 docker-ce | 5:18.09.0~3-0~debian-stretch | https://download.docker.com/linux/debian stretch/stable armhf Packages
 docker-ce | 18.06.1~ce~3-0~debian | https://download.docker.com/linux/debian stretch/stable armhf Packages
 docker-ce | 18.06.0~ce~3-0~debian | https://download.docker.com/linux/debian stretch/stable armhf Packages
 docker-ce | 18.03.1~ce-0~debian | https://download.docker.com/linux/debian stretch/stable armhf Packages
 docker-ce | 18.03.0~ce-0~debian | https://download.docker.com/linux/debian stretch/stable armhf Packages
 docker-ce | 17.12.1~ce-0~debian | https://download.docker.com/linux/debian stretch/stable armhf Packages
 docker-ce | 17.12.0~ce-0~debian | https://download.docker.com/linux/debian stretch/stable armhf Packages
 docker-ce | 17.09.1~ce-0~debian | https://download.docker.com/linux/debian stretch/stable armhf Packages
 docker-ce | 17.09.0~ce-0~debian | https://download.docker.com/linux/debian stretch/stable armhf Packages
 docker-ce | 17.06.2~ce-0~debian | https://download.docker.com/linux/debian stretch/stable armhf Packages
 docker-ce | 17.06.1~ce-0~debian | https://download.docker.com/linux/debian stretch/stable armhf Packages
 docker-ce | 17.06.0~ce-0~debian | https://download.docker.com/linux/debian stretch/stable armhf Packages
```
Install the specific version
```shell
pi@armboard:~ $ sudo apt-get update && sudo apt-get install docker-ce=5:18.09.0~3-0~debian-stretch
```
Or try this one for Rapbian
```shell
pi@armboard:~ $ sudo apt-get purge docker-ce
pi@armboard:~ $ sudo apt-get install docker-ce=18.06.3~ce~3-0~raspbian
pi@armboard:~ $ sudo systemctl enable docker && sudo systemctl start docker
```

6. Avoid using docker with root

Adding a user to the "docker" group will grant the ability to run containers which can be used to obtain root privileges on the docker host.If you would like to use Docker as a non-root user, you should now consider adding your user to the "docker" group with something like :
```shell
pi@armboard:~ $ sudo usermod -aG docker $USER
```

### Docker Compose install with pip
After some time using Docker, one of the first tools you will need from Docker universe is docker-compose, So it is better to have it installed now. Just type :
```shell
pi@armboard:~ $ sudo apt-get -y install python-pip
pi@armboard:~ $ sudo -H pip install --upgrade pip
pi@armboard:~ $ sudo -H pip install setuptools
pi@armboard:~ $ sudo -H pip install docker-compose
```

If everything goes well, you should be able to check Docker-compose version in console.
```shell
pi@armboard:~ $ docker-compose version
```

## Application initialisation and configuration

1. Build and run the cluster
```bash
user@raspberry:~/docker_path$ docker-compose build --no-cache && docker-compose up --build -d
```

2. Change Postgre user password
```bash
user@raspberry:~/docker_path$ docker exec -ti postgresql /bin/sh
```
```bash
root@postgresql_container:~/# su postgres
```
```bash
postgres@postgresql_container:~/# psql
```
```bash
postgres=# \password
Enter new password: 
Enter it again:
postgres=# \q
```

3. Bootstrap the DB
```bash
user@raspberry:~/docker_path$ docker-compose run --rm python /bin/sh -c "cd /opt/services/flaskapp/src && python -c  'import database; database.init_db()'"
```

4. Browse to localhost:8080 to see the app in action.


5. Browse to localhost:8081 to access admin.