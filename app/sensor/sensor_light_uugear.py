import RPi.GPIO as GPIO

class Light:

    PIR = 4

    # Light sensor configuration
    def __init__(self, pirValue=None):
        if pirValue is not None:
            self.PIR = pirValue
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.PIR, GPIO.IN)

    def getState(self):
        return GPIO.input(self.PIR)

'''
# Usage examples
print(Light().getState());
'''
