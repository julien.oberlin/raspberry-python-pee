import smbus2
import bme280
import json

class Bme280:

    port = 1
    address = 0x77
    bus = ''
    calibration_params = ''

    def __init__(self, port=None, address=None):
        if port is not None:
            self.port = port
        if address is not None:
            self.address = address
        self.bus = smbus2.SMBus(self.port)
        self.calibration_params = bme280.load_calibration_params(self.bus, self.address)

    def getId(self):
        data = self.getSensorData();
        return str(data.id);

    def getTimeStamp(self):
        data = self.getSensorData();
        return str(data.timestamp.strftime("%Y-%m-%d %H:%M:%S"));

    def getTemperature(self):
        data = self.getSensorData();
        return int(data.temperature);

    def getHumidity(self):
        data = self.getSensorData();
        return int(data.humidity);

    def getPressure(self):
        data = self.getSensorData();
        return int(data.pressure);

    def getSensorDataJson(self):
        return json.dumps({'timestamp': self.getTimeStamp(), 'temperature': self.getTemperature(), 'humidity': self.getHumidity(), 'pressure': self.getPressure()})

    def getSensorData(self):
        return bme280.sample(self.bus, self.address, self.calibration_params)

'''
# Usage examples
print(Bme280().getTemperature())
print(Bme280().getHumidity())
print(Bme280().getPressure())
print(Bme280().getSensorDataJson())
'''
