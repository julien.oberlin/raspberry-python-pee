import RPi.GPIO as GPIO
import time
import datetime
from datetime import datetime
from datetime import date

class HCSrc501:

    PIR = 7

    state = 0

    def __init__(self, pirValue=None):

        if pirValue is not None:
            self.PIR = pirValue

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.PIR, GPIO.IN)
        '''
        try:
            def callbackChangeState(channel):
                self.changeState(1)
            GPIO.add_event_detect(self.PIR, GPIO.RISING, callback=callbackChangeState)
            while 1:
                self.changeState(0)
                time.sleep(100)
        except KeyboardInterrupt:
            print(" Cleaning up the GPIO")
            GPIO.cleanup()
        '''

    def changeState(self, newState):
        if 0 == newState:
            print("No movement")
        else:
            print("Movement detected")
        self.state = newState

    def getState(self):
        return GPIO.input(self.PIR)

'''
# Usage examples
move = HCSrc501()
while 1:
    if move.getState() == 0:
        print("No move")
    else:
        print("Move detected")
'''
