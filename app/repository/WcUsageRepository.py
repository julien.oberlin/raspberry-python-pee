from datetime import datetime, date

from database import Session, engine, Base
from model.WcUsage import WcUsage
from sqlalchemy import Date, cast, asc, desc

class WcUsageRepository:

    __database = None
    __engine = None
    __session = None
    __model = None
    __query = None

    def __init__(self):
        self.__database = Base
        self.__engine = engine
        self.__session = Session()
        self.__wcUsage = WcUsage
        self.__query = self.__session.query(self.__wcUsage)

    def getDatabase(self):
        return self.__database

    def getEngine(self):
        return self.__engine

    def getLastWcUsage(self):
        return self.__query.order_by(self.__wcUsage.id.desc()).first()

    def getInTimeForLastOutTimeNone(self):
        return self.__query.filter(self.__wcUsage.out_datetime.is_(None)).order_by(self.__wcUsage.id.desc()).first()

    def getTodayUsagesByType(self, wc_usage_type):
        return self.__query.filter_by(usage_type=wc_usage_type).filter(cast(self.__wcUsage.in_datetime, Date) == date.today()).order_by(self.__wcUsage.in_datetime).all()

    def getByUsageTypeForToday(self, usageType):
        return self.__query.filter_by(usage_type=usageType).filter(cast(WcUsage.in_datetime, Date) == date.today()).order_by(asc(WcUsage.in_datetime)).all()

    def save(self, model):
        self.__session.add(model)
        self.__session.commit()
        self.__session.flush()
        self.__session.refresh(model)
        #self.__session.close()

    def update(self, model, data):
        self.__query.filter(self.__wcUsage.id == model.id).update(data)
        self.__session.commit()
