from database import Database

class Repository:

    query = None
    model = None

    def __init__(self, model):
        database = Database()
        self.model = model
        self.query = database.db_session.query(self.model)
