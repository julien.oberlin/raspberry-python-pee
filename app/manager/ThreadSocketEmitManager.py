from threading import Thread, Event
from time import sleep

class ThreadSocketEmitManager(Thread):

    __socketIo = None

    __thread_stop_event = None

    __emitName = None

    __callbackEmitData = None

    __callbackOnData = None

    __delay = 1

    def __init__(self, socketio, emitName, callbackEmitData = None, callbackOnData = None):
        self.__thread_stop_event = Event()
        self.__socketIo = socketio
        self.__emitName = emitName
        self.__callbackEmitData = callbackEmitData
        self.__callbackOnData = callbackOnData
        super(ThreadSocketEmitManager, self).__init__()

    def setDelay(self, delay):
        self.__delay = delay
        return self

    def getDelay(self):
        return self.__delay

    def initLoop(self):
        #infinite loop
        while not self.__thread_stop_event.isSet():
            if hasattr(self.__callbackEmitData, '__call__'):
                emitdata = self.__callbackEmitData()
            else:
                emitdata = {}

            if hasattr(self.__callbackOnData, '__call__'):
                emitdata = self.__callbackOnData(emitdata)

            self.__socketIo.emit(self.__emitName, emitdata, namespace='/' + str(self.__emitName))

            sleep(self.__delay)

    def run(self):
        self.initLoop()
