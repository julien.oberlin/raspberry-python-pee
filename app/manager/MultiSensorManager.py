from sensor.sensor_moist_BME280 import Bme280

class MultiSensorManager:

    __multiSensor = None

    def __init__(self, port=None, address=None):
        self.__multiSensor = Bme280(port, address)

    def getTemperature(self):
        return self.__multiSensor.getTemperature()

    def getHumidity(self):
        return self.__multiSensor.getHumidity()

    def getPressure(self):
        return self.__multiSensor.getPressure()

    def getSensorDataJson(self):
        return self.__multiSensor.getSensorDataJson()
'''
# Usage examples
print(Light(6).isLightOn());
'''
