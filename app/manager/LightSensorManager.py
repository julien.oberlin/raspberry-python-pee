import os

from sensor.sensor_light_uugear import Light

class LightSensorManager:

    __lightSensor = None

    def __init__(self, pirValue=None):
        self.__lightSensor = Light(pirValue)

    def isLightOn(self):
        return True if self.__lightSensor.getState() == 0 else False

'''
# Usage examples
print(Light(6).isLightOn());
'''