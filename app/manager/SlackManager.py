from urllib import request, parse
import json

class SlackManager:

    __wekbookUrl = 'https://hooks.slack.com/services/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'
    __channelName = None
    __userName = None
    __emoji = None

    def __init__(self):
        self.__channelName = '#wc'
        self.__userName = 'HARPIC'
        self.__emoji = ':restroom:'

    def setWekbookUrl(self, wekbookUrl):
        self.__wekbookUrl = wekbookUrl
        return self

    def getWekbookUrl(self):
        return self.__wekbookUrl

    def setChannelName(self, channelName):
        self.__channelName = channelName
        return self

    def getChannelName(self):
        return self.__channelName

    def setUserName(self, userName):
        self.__userName = userName
        return self

    def getUserName(self):
        return self.__userName

    def setEmoji(self, emoji):
        self.__emoji = emoji
        return self

    def getEmoji(self):
        return self.__emoji

    def sendMessage(self, message):
        data = {
            'channel': self.__channelName,
            'text': message,
            'username': self.__userName,
            'icon_emoji': self.__emoji,
        }
        json_data = json.dumps(data)
        req = request.Request(self.getWekbookUrl(), data=json_data.encode('ascii'), headers={'Content-Type': 'application/json'}) 
        return request.urlopen(req)
