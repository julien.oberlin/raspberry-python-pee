import os
import math
import json
from time import sleep, strftime
from datetime import datetime, date

from model.WcUsage import WcUsage
from value_object.WcUsageValueObject import WcUsageValueObject
from manager.SlackManager import SlackManager
from manager.LightSensorManager import LightSensorManager
from manager.MultiSensorManager import MultiSensorManager
from repository.WcUsageRepository import WcUsageRepository

class WcUsageManager:

    __socketIo = None
    __lightSensorManager = None
    __multiSensorManager = None
    __wcUsageRepository = None

    __status = None
    __lastWcUsage = None
    __starting = True

    def __init__(self, socketIo):
        self.__socketIo = socketIo
        self.__lightSensorManager = LightSensorManager()
        self.__multiSensorManager = MultiSensorManager()
        self.__wcUsageRepository = WcUsageRepository()

    def getRepository(self):
        return self.__wcUsageRepository

    def getSensorsData(self):
        light = self.__lightSensorManager.isLightOn()
        state = "occupied" if light else "free"
        temperature = self.__multiSensorManager.getTemperature()
        humidity = self.__multiSensorManager.getHumidity()
        pressure = self.__multiSensorManager.getPressure()
        return {'light': light, 'state': state, 'pressure': str(int(pressure)), 'humidity': str(int(humidity)), 'temperature': str(int(temperature) - 10), 'datetime': strftime("%Y-%m-%d %H:%M:%S")}

    def createTableIfDontExist(self):
        engine = self.__wcUsageRepository.getEngine()
        if not engine.dialect.has_table(engine, WcUsage.__tablename__): 
            print(strftime("%Y-%m-%d %H:%M:%S") + ' : Create table' + WcUsage.__tablename__)
            self.createTable()

    def createTable(self):
        self.__wcUsageRepository.getDatabase().metadata.create_all(self.__wcUsageRepository.getEngine())

    def addWcUsage(self, WcUsage):
        self.__wcUsageRepository.save(WcUsage)
        return self

    def updateWcUsage(self, WcUsage, data):
        self.__wcUsageRepository.update(WcUsage, data)
        return self

    def updateStatusOnEmit(self, data):
        if data['light'] != self.__status and data['light']:
            self.__status = data['light']
            wc_usage = WcUsage(None, datetime.now())
            self.addWcUsage(wc_usage)
            self.__lastWcUsage = wc_usage
            message = self.getLastWcUsageMessage(data)
            SlackManager().sendMessage(message)
            print(strftime("%Y-%m-%d %H:%M:%S") + ' : Add new DB entry with wc usage start datetime')

        elif not data['light'] and self.__status:
            self.__status = data['light']
            wcusage_type = self.getWcUsageTypeFromDates(self.__lastWcUsage.in_datetime, datetime.now())
            self.updateWcUsage(self.__lastWcUsage, {'usage_type': wcusage_type, 'out_datetime': datetime.now()})

            wcUsageData = self.getWcUsageDataByUsageType(wcusage_type)
            self.__socketIo.emit('chart', {wcusage_type: wcUsageData}, namespace='/socket')

            message = self.getLastWcUsageMessage(data, self.__lastWcUsage)
            SlackManager().sendMessage(message)
            self.__lastWcUsage = None
            print(strftime("%Y-%m-%d %H:%M:%S") + ' : Update last DB entry with wc usage end datetime')
        
        if(self.__status is None and False == data['light'] and self.__starting):
            message = self.getLastWcUsageMessage(data)
            SlackManager().sendMessage(message)
            self.__starting = False
         
        if self.__lastWcUsage is not None:
            data['start_datetime'] = self.__lastWcUsage.in_datetime.strftime("%Y-%m-%d %H:%M:%S")

        return data

    def getLastWcUsage(self):
        return self.__wcUsageRepository.getLastWcUsage()

    def getWcUsageTypeFromDates(self, inDatetime, outDateTime):
        delay = outDateTime - inDatetime
        seconds = delay.total_seconds()
        if (seconds > 180):
            return WcUsage.TYPE_POOP
        else:
            return WcUsage.TYPE_PEE

    def getLastWcUsageMessage(self, data, lastWcUsage = None):
        delayMessage = ''
        state = "libres" if data['state'] == 'free' else "occupés"
        signal = ":arrow_up:" if data['state'] == 'free' else ":no_entry:"
        if lastWcUsage is not None:
            delay = lastWcUsage.out_datetime - lastWcUsage.in_datetime;
            seconds = delay.total_seconds()
            minutes = int(seconds / 60) % 60
            stringTime = str(seconds) + " secondes" if minutes == 0 else str(minutes) + " minutes"
            if minutes > 2:
                delayMessage = '(dernière occupation de ' + stringTime + ' :biohazard_sign: :poop:)'
            else:
                delayMessage = '(dernière occupation de ' + stringTime + ' :rose: :+1:)'
        return str(signal) + ' ' + strftime("%Y-%m-%d %H:%M:%S") + ' : les toilettes sont actuellement ' + str(state) + ' ! ' + delayMessage

    def getWcUsageDataByUsageType(self, usageType):
        wcUsageValues = []
        wcUsageData = self.__wcUsageRepository.getByUsageTypeForToday(usageType)
        for value in wcUsageData:
            if value.out_datetime is not None and value.in_datetime is not None:
                delay = value.out_datetime - value.in_datetime;
                seconds = delay.total_seconds()
                hours = int(seconds / 3600)
                minutes = int(seconds / 60) % 60
                inDatetimeMinute = (str(value.in_datetime.minute), '0' + str(value.in_datetime.minute))[1 == len(str(value.in_datetime.minute))]
                wcUsageValueObject = WcUsageValueObject(str(value.in_datetime.hour) + ':' + inDatetimeMinute, int(seconds / 60))
                wcUsageValues.append(wcUsageValueObject)
        return json.dumps([data.toJSON() for data in wcUsageValues])
