from database import Base
from sqlalchemy import MetaData, Table, Column, Integer, String, Enum
from sqlalchemy.types import DateTime

class WcUsage(Base):

    TYPE_POOP='poop'
    TYPE_PEE='pee'

    __tablename__ = 'wc_usage'

    id = Column('id', Integer, primary_key=True)

    usage_type = Column('usage_type', Enum('poop', 'pee', name='use_type'), nullable=True)

    in_datetime = Column('in_datetime', DateTime(), nullable=False)

    out_datetime = Column('out_datetime', DateTime(), nullable=True)

    def __init__(self, usage_type = None, in_datetime = None, out_datetime = None):
        self.usage_type = usage_type
        self.in_datetime = in_datetime
        self.out_datetime = out_datetime
