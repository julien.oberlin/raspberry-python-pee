$(document).ready(function(){

    var socket = io('http://192.168.0.16');
    socket.on('newnumber', function (data) {
        console.log(data);
        socket.emit('my other event', { my: 'data' });
    });


    //connect to the socket server.
    var socket = io.connect(location.protocol + '//' + location.hostname + ':' + location.port + '/test');
    var numbers_received = [];

    //receive details from server
    socket.on('newnumber', function(msg) {
        console.log("Received number : " + msg.number);
        //maintain a list of ten numbers
        if (numbers_received.length >= 10){
            numbers_received.shift()
        }
        numbers_received.push(msg.number);
        numbers_string = '';
        for (var i = 0; i < numbers_received.length; i++){
            numbers_string = numbers_string + '<p>' + numbers_received[i].toString() + '</p>';
        }
        $('#log').html(numbers_string);
    });

});