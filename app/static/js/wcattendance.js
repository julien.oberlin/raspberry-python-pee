(function ($) {
	$.fn.timeChartline = function(options) {

		var that = this;

		var chartColors = {
			red: 'rgb(255, 99, 132)',
			orange: 'rgb(255, 159, 64)',
			yellow: 'rgb(255, 205, 86)',
			green: 'rgb(75, 192, 192)',
			blue: 'rgb(54, 162, 235)',
			purple: 'rgb(153, 102, 255)',
			grey: 'rgb(231,233,237)',
			maroon: 'rgb(165,42,42)',
			saddlebrown: 'rgb(139,69,19)',
		};

		var functions =  {
			ajaxDisplay: function(url, label, title, color) {
				$.get(url, function(data) {
					functions.display(JSON.parse(data), label, title, color);
				});
			},
			display: function(json, label, title, color) {
				console.log(json);
				var labels = json.map(e => moment(e.x, 'HH:mm').format("HH:mm"));
				var data = json.map(e => +e.y);
				var ctx = document.getElementById(that.attr('id')).getContext('2d');

				var options = {
					type: 'line',
					data: {
						labels: labels,
						datasets: [{
							label: label,
							backgroundColor: chartColors[color],
							borderColor: chartColors[color],
							data: data,
							fill: false,
							borderWidth: 1
						}]			  
					},
					options: {
						responsive: true,
						title: {
							display: true,
							text: title
						},
						scales: {
							xAxes: [{
								display: true,
								scaleLabel: {
									display: true,
									labelString: 'Heure'
								},
								type: 'time',
								time: {
									format: "HH:mm",
									unit: 'hour',
									unitStepSize: 2,
									displayFormats: {
									  'minute': 'HH:mm',
									  'hour': 'HH:mm',
									  min: '00:00',
									  max: '23:59',
									},
								}
							}],
							yAxes: [{
								display: true,
								scaleLabel: {
									display: true,
									labelString: 'Durée en minute'
								},
							}],
						},
					}
				};
				
				var myChart = new Chart(ctx, options);
			},
		};
		return functions;
	}
}( jQuery ));
