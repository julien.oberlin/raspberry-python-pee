# -*- coding: utf-8 -*-

# If eventlet or gevent are used, then monkey patching the Python standard library is normally required
# to force the message queue package to use coroutine friendly functions and classes.
import eventlet
eventlet.monkey_patch()

import os
import requests
import RPi.GPIO as GPIO
import time
from time import sleep, strftime
from random import random
from threading import Thread, Event
from datetime import datetime, date
from flask import Flask, render_template, redirect, url_for, jsonify
from flask_socketio import SocketIO, emit

### App own classes
from model.WcUsage import WcUsage
from manager.WcUsageManager import WcUsageManager
from manager.ThreadSocketEmitManager import ThreadSocketEmitManager

### Creating a flask app and using it to instantiate a socket object
app = Flask(__name__)
app.secret_key = os.environ['APP_SECRET_KEY']
app.config['DEBUG'] = os.environ['FLASK_DEBUG']

### Set time-zone
os.environ['TZ'] = 'Europe/Paris'
time.tzset()

### Turn the flask app into a socketio app
socketio = SocketIO(app, async_mode = 'eventlet')

### Initiate thread
thread = Thread()

@app.before_first_request
def activate_job():
    print(strftime("%Y-%m-%d %H:%M:%S") + ' : WcUsage sensors starting')
    # need visibility of the global thread object
    global thread

    # Create table on launch if don't exist
    wcUsageManager = WcUsageManager(socketio)
    wcUsageManager.createTableIfDontExist()

    # Start the random number generator thread only if the thread has not been started before.
    if not thread.isAlive():
        thread = ThreadSocketEmitManager(socketio, 'socket', wcUsageManager.getSensorsData, wcUsageManager.updateStatusOnEmit)
        thread.start()

### Handler for default flask route
@app.route('/')
def index():
	return render_template('index.html')

### On socket client connection
@socketio.on('connect', namespace='/socket')
def connect():
    print(strftime("%Y-%m-%d %H:%M:%S") + ' : HTTP client connected')

### on page load event triggered when the HTML page is loaded
@socketio.on('onpageload', namespace='/socket')
def onpageload(json, methods=['GET', 'POST']):
    wcUsagePoopData = WcUsageManager(socketio).getWcUsageDataByUsageType(WcUsage.TYPE_POOP) 
    socketio.emit('chart', {WcUsage.TYPE_POOP: wcUsagePoopData}, namespace='/socket')
    wcUsagePeeData = WcUsageManager(socketio).getWcUsageDataByUsageType(WcUsage.TYPE_PEE) 
    socketio.emit('chart', {WcUsage.TYPE_PEE: wcUsagePeeData}, namespace='/socket')

### on socket client disconnection
@socketio.on('disconnect', namespace='/socket')
def disconnect():
    print(strftime("%Y-%m-%d %H:%M:%S") + ' : HTTP client disconnected')

### start runner to reach the app page first to initiate sensors cahnges' emit
def start_runner():
    def start_loop():
        not_started = True
        while not_started:
            print(strftime("%Y-%m-%d %H:%M:%S") + ' : Start loop for WcUsage init')
            try:
                r = requests.get('http://nginx')
                if r.status_code == 200:
                    print(strftime("%Y-%m-%d %H:%M:%S") + ' : WcUsage started, quiting start loop')
                    not_started = False
                print(r.status_code)
            except:
                print(strftime("%Y-%m-%d %H:%M:%S") + ' : WcUsage not yet started')
            sleep(2)

    print(strftime("%Y-%m-%d %H:%M:%S") + ' : WcUsage runner started')
    thread = Thread(target=start_loop)
    thread.start()

### Notice how socketio.run takes care of app instantiation as well.
if __name__ == '__main__':
    start_runner()
    socketio.run(app, host='0.0.0.0')
