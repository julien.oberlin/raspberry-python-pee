# -*- coding: utf-8 -*-

from flask_socketio import SocketIO, emit
from manager.WcUsageManager import WcUsageManager
WcUsageManager(SocketIO).createTable()
